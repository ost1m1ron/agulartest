import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fist',
  templateUrl: './fist.component.html',
  styleUrls: ['./fist.component.scss']
})
export class FistComponent implements OnInit {
  firstIndex :number= 0;
  lastIndex : number = 0;
  bool : boolean = false;
  public textProp : string = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum";
  public textArray: { text:string, visible:boolean }[] = [];
  public speed:number = 300;
  constructor() { }

  ngOnInit(): void {
    //this.text = this.ConverToTag(this.text);

  }
  ConverToTag(text:string):string{
    //let splitedText = this.text.split(' ');
    let speed = 3;
    // let asd = splitedText.forEach(element => {
    //   element = "<span style='opacity: 0; transition: all 0.3s ease 0s;'>" + element + " " + "</span>"
    // });
    //splitedText.forEach((val, index) => splitedText[index] = "<span style='opacity: 0; transition: all 0.3s ease 0s;'>" + val + " " + "</span>");
    //return splitedText.join(" ");
    return "";
  }

  Hide(){    
    // let lenght = this.text.split(' ')[0].length;
    // let whiteSpace = ' '.repeat(lenght  + 1);
    // console.log(whiteSpace.length);
    // console.log(this.text);
    // '<span style="opacity: 0; transition: all 0.3s ease 0s;">состоящая </span>'
    // this.text = whiteSpace + this.text.split(' ').slice(1).join(' ');
    // this.text = this.ConverToTag(this.text);\
    
    
    this.textArray = this.textProp.split(" ").map(el => { return { text:el, visible : true }});
    let index = 0;
    let lenght = this.textArray.length;
    const interval = 
    setInterval(() => {
        this.textArray[index].visible = false;
        console.log(this.textArray[index]);
        index++;
        if(index == lenght){
          clearInterval(interval);
        }
    }, this.speed    
    );
  }

}

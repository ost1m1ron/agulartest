import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import {Howl, Howler} from 'howler';
import { scoreDto } from '../../interfaces/scoreDto.interface';
import { baseDirDto } from '../../interfaces/baseDirDto.interface';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-flesh-row',
  templateUrl: './flesh-row.component.html',
  styleUrls: ['./flesh-row.component.scss'],
  animations: [
    trigger('flipState', [
      state('active', style({
        transform: 'rotateY(179deg)'
      })),
      state('inactive', style({
        transform: 'rotateY(0)'
      })),
      transition('active => inactive', animate('500ms ease-out')),
      transition('inactive => active', animate('500ms ease-in'))
    ])
  ]
})

export class FleshRowComponent implements OnInit {
  private id: number | undefined;
  private type: number | undefined;
  private subscription: Subscription;

  //свойства настройки
  trainingTypes : baseDirDto[] = [{id : 1, value : "Умножение"}, {id : 2, value : "Деление"}];  
  firstNumbers : number[] = [1,2,3,4,5,6,7,8];
  secondNumbers : number[] = [1,2,3,4,5,6,7,8];
  public score : scoreDto = {count : 0, value : 0};
  public D : number = 0;

  trainingType : number = 1;
  firstNumberDiv : number = 1;
  secondNumberDiv : number = 1;

  first : number = 1;
  second : number = 1;
  stringType : string = "*";
  public question: number[] = [];
  public answer : number = 1;
  
  displayTimer : string = "";
  isAnswer : boolean = false;
  public isRunning: boolean = true;
  time = 0;
  public answerTime: number = 0;

  // audio = new Audio('@../../assets/soundfail_1.mp3');
  failSounds = ['fail_1.mp3', 'fail_1.mp3', 'fail_1.mp3'];
  fail = new Howl({src: ['assets/fail_1.mp3']});
  success = new Howl({src: ['assets/success_1.mp3']});
  public isPlaySound : boolean = true;
  public questionText : string = "";
  
  constructor(private activateRoute: ActivatedRoute) { 
    this.subscription = activateRoute.params.subscribe(params=>{this.id=params['id'], this.type=params['type']});
  }
  

  ngOnInit(): void {
    // если если пришел Id то нужно стянуть с сервера задание    
    this.stopwatch();
  }

  stopwatch() {
    timer(0, 1000).subscribe(ellapsedCycles => {
      if (this.isRunning) {
        this.time++;
        this.getDisplayTimer(this.time);
      }
    });
  }

getDisplayTimer(time: number) {
    let minutes = '0' + Math.floor(time % 3600 / 60);
    let seconds = '' + Math.floor(time % 3600 % 60);  
    Number(seconds) < 10 ? seconds = '0' + seconds : seconds = '' + seconds; 
   
    this.displayTimer = minutes + ':' + seconds;
  }

  private GetValueId($event:any):number{
    return $event.target.value;
  }

  SelectType($event:any){
    this.trainingType = this.GetValueId($event);  
    this.trainingType == 1 ? this.stringType = "*" : this.stringType = "÷";
  }

  SelectD($event:any){
    this.D = this.GetValueId($event);  
  }

  SelectFistNumber($event:any){
    this.firstNumberDiv = this.GetValueId($event);  
  }

  SelectSecondNumber($event:any){
    this.secondNumberDiv = this.GetValueId($event);  
  }

  Start(){
    this.Reset();    
  }

  Reset(){    
    this.question = [];    

    for(let i = 0; i < this.D; i++){
      let asd = this.GetRanomNumber(this.firstNumberDiv);
      console.log(asd);
      this.question.push(asd);
      console.log(this.question);
    }    
    
    this.time = 0;
    this.isRunning = true;
  }

  ReStart(){
    this.time = 0;
    this.Reset();
  }

  Stop(){
    console.log("Stop");
  }

  GetRanomNumber(n:number):number{   
    const max =  Math.pow(10, n) - 1; //  9, 99, 999, 9999
    const min = Math.pow(10, n) / 10 ; // 1, 10, 100, 1000
    const plusOrMinus = Math.random() < 0.5 ? -1 : 1; // -1 or +1
    return Math.floor(Math.random() * max + min) * plusOrMinus;
  }

  Answer(){
    if(this.isAnswer){
      this.score.count++;
      if(this.question.reduce((a, b) => a + b) == this.answer){        
        if(this.isPlaySound)
          this.success.play()
      }else{
        if(this.isPlaySound)
          this.fail.play();  
      } 
      this.answerTime = this.time;
      this.Reset();   
      this.isAnswer = false;
    }else{
      if(this.question.reduce((a, b) => a - b) == this.answer){
        this.score.value++;
        this.questionText = this.questionText + " = " + this.answer;     
      }         
      else
        this.questionText = this.questionText + " ≠ " + this.answer;      
      this.isAnswer = true;
    }        
  }
  
  public flip: string = 'inactive';

  toggleFlip() {
    this.flip = (this.flip == 'inactive') ? 'active' : 'inactive';
  }
}

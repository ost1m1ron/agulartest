import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FleshOneComponent } from './flesh-one.component';

describe('FleshOneComponent', () => {
  let component: FleshOneComponent;
  let fixture: ComponentFixture<FleshOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FleshOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FleshOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

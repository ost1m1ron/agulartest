import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import {baseDirDto} from '../interfaces/baseDirDto.interface';
import {Howl, Howler} from 'howler';
import { scoreDto } from '../interfaces/scoreDto.interface';


@Component({
  selector: 'app-flesh-one',
  templateUrl: './flesh-one.component.html',
  styleUrls: ['./flesh-one.component.scss']
})

export class FleshOneComponent implements OnInit {
  private id: number | undefined;
  private type: number | undefined;
  private subscription: Subscription;

  //свойства настройки
  trainingTypes : baseDirDto[] = [{id : 1, value : "Умножение"}, {id : 2, value : "Деление"}];  
  firstNumbers : number[] = [1,2,3,4,5,6,7,8];
  secondNumbers : number[] = [1,2,3,4,5,6,7,8];
  public score : scoreDto = {count : 0, value : 0};

  trainingType : number = 1;
  firstNumberDiv : number = 1;
  secondNumberDiv : number = 1;

  first : number = 1;
  second : number = 1;
  stringType : string = "*";
  question: number = 0;
  public answer : number = 1;
  
  displayTimer : string = "";
  isAnswer : boolean = false;
  public isRunning: boolean = true;
  time = 0;
  public answerTime: number = 0;

  // audio = new Audio('@../../assets/soundfail_1.mp3');
  failSounds = ['fail_1.mp3', 'fail_1.mp3', 'fail_1.mp3'];
  fail = new Howl({src: ['assets/fail_1.mp3']});
  success = new Howl({src: ['assets/success_1.mp3']});
  public isPlaySound : boolean = true;
  public questionText : string = "";
  
  constructor(private activateRoute: ActivatedRoute) { 
    this.subscription = activateRoute.params.subscribe(params=>{this.id=params['id'], this.type=params['type']});
  }
  

  ngOnInit(): void {
    // если если пришел Id то нужно стянуть с сервера задание    
    this.stopwatch();
  }

  stopwatch() {
    timer(0, 1000).subscribe(ellapsedCycles => {
      if (this.isRunning) {
        this.time++;
        this.getDisplayTimer(this.time);
      }
    });
  }

getDisplayTimer(time: number) {
    let minutes = '0' + Math.floor(time % 3600 / 60);
    let seconds = '' + Math.floor(time % 3600 % 60);  
    Number(seconds) < 10 ? seconds = '0' + seconds : seconds = '' + seconds; 
   
    this.displayTimer = minutes + ':' + seconds;
  }

  private GetValueId($event:any):number{
    return $event.target.value;
  }

  SelectType($event:any){
    this.trainingType = this.GetValueId($event);  
    this.trainingType == 1 ? this.stringType = "*" : this.stringType = "÷";
  }

  SelectFistNumber($event:any){
    this.firstNumberDiv = this.GetValueId($event);  
  }

  SelectSecondNumber($event:any){
    this.secondNumberDiv = this.GetValueId($event);  
  }

  Start(){
    this.Reset();    
  }

  Reset(){
    this.first = this.GetRanomNumber(this.firstNumberDiv);
    this.second = this.GetRanomNumber(this.secondNumberDiv);
    if(this.trainingType == 1){
      this.question = this.first * this.second;
    }else{
      this.question = this.first / this.second;
    }
    this.questionText = this.first + " " + this.stringType + " " + this.second;
    this.time = 0;
    this.isRunning = true;
  }

  ReStart(){
    this.time = 0;
    this.Reset();
  }

  Stop(){
    console.log("Stop");
  }

  GetRanomNumber(n:number):number{   
    const max =  Math.pow(10, n) - 1; //  9, 99, 999, 9999
    const min = Math.pow(10, n) / 10 ; // 1, 10, 100, 1000
    return Math.floor(Math.random() * max + min);
  }

  Answer(){
    if(this.isAnswer){
      this.score.count++;
      if(this.question == this.answer){        
        if(this.isPlaySound)
          this.success.play()
      }else{
        if(this.isPlaySound)
          this.fail.play();  
      } 
      this.answerTime = this.time;
      this.Reset();   
      this.isAnswer = false;
    }else{
      if(this.question == this.answer){
        this.score.value++;
        this.questionText = this.questionText + " = " + this.answer;     
      }         
      else
        this.questionText = this.questionText + " ≠ " + this.answer;      
      this.isAnswer = true;
    }
        
  }
  
}

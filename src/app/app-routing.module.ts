import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FleshOneComponent } from './game/mental/flesh-one/flesh-one.component';
import { FleshRowComponent } from './game/mental/flesh-row/flesh-one/flesh-row.component';
import { FistComponent } from './game/reading/fist/fist.component';
import { MainComponent } from './main/main/main.component';

const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  {path: 'game/flesh-one/:id', component: FleshOneComponent},
  {path: 'game/flesh-one/:id/:type', component: FleshOneComponent},
  {path: 'game/first', component: FistComponent},
  {path: 'game/flesh-row', component: FleshRowComponent},
  {path : 'main', component: MainComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

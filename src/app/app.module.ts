import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FleshOneComponent } from './game/mental/flesh-one/flesh-one.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FistComponent } from './game/reading/fist/fist.component';
import { MainComponent } from './main/main/main.component'
import { FleshRowComponent } from './game/mental/flesh-row/flesh-one/flesh-row.component';

@NgModule({
  declarations: [
    AppComponent,
    FleshOneComponent,
    FleshRowComponent,
    FistComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
